import React, { Component } from 'react'
import GoogleMap from 'google-map-react'

import './App.css';
import setMapOptions from './mapOptions';
import Card from './components/Card';
import Marker from './components/Marker';
import Legend from './components/Legend';
import Pagination from './components/Pagination';

var keys = Object.keys || require('object-keys');

Object.keys = function(obj) {
    keys = [];

    for (var i in obj) {
      if (obj.hasOwnProperty(i)) {
        keys.push(i);
      }
    }

    return keys;
};

var regions = {
    "0" : "Asia-Pacific",
    "1" : "Europe, America, Latin America and Russia",
    "2" : "Pacific Islands",
    "3" : "Middle East and Africa"
};

class App extends Component {
    static defaultProps = {
        // set an initial map view
        center: { lat: 20.797201434307, lng: 22.8125 },
        zoom: 1
    }

    state = {
        filter: {},
        projects: [],
        types: [],
        pTyps: [],
        center: [20.797201434307, 22.8125],
        zoom: 3,
        expanded: true,
        currentPage: 1,
        projectsPerPage: 3,
        pageTotal: 0
    }

    componentWillMount() {
        /*
         * !!! Requires Custom Map data and Google Maps API key !!!
         * A sample data structure can be found on `data.json` file
         */

        //this.apiURL = window.MAPS_API_URL || 'http://www.mocky.io/v2/59a452b91000007f01b2aaea';
        //this.apiURL = window.MAPS_API_URL || 'http://www.mocky.io/v2/59ae52a01300008e04035691';
        //this.apiURL = window.MAPS_API_URL || 'http://www.mocky.io/v2/59e99645100000ce00135daf';
        this.apiURL = window.MAPS_API_URL || 'https://api.myjson.com/bins/1bh3ln';
        this.apiKey = window.MAPS_API_KEY || 'AIzaSyAhF9gZqAM0uksoaEJgMSglOLktMxrAhKg';

        // fetch json from api, please take note of the response format
        fetch(this.apiURL)
            .then(res => res.json())
            .then((data) => {
                this.apiData = data.body
                this.setState({'projects': data.body});
                this.polylines = []
                this.buildFilterOptions()

                this.apiData.forEach( project => {
                    this._drawPolylines(project.paths, project.completed);
                });

                const pageTotal = Math.ceil(this.state.projects.length / this.state.projectsPerPage);
                this.setState({ pageTotal });
            }
        )

    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.projects !== this.state.projects) {
            this.resetPagination();
        }
    }

    // dynamically build select options for filters
    buildFilterOptions = () => {
        const locs = [];
        const typs = [];
        const pTyps = [];

        // // build locations dropdown options
        // this.apiData.forEach( project => {
        //     project.locations.forEach( location => {
        //     if (locs.some( loc => loc.id === location.id )) return
        //         locs.push(location)
        //     })
        // });

        // build project type dropdown options
        this.apiData.forEach( project => {

            if (pTyps.indexOf(project.projectType) < 0) {
                pTyps.push(project.projectType);
            }
        });

        this.apiData.forEach( project => {
            if (typs.indexOf(project.type) < 0) {
                typs.push(project.type);
            }
        });

        this.setState({ locations: locs, types: typs, pTyps: pTyps });
        this.onMapInit()
    }

    // ran after setting state filters
    filterProjects = () => {

        const { projectType, type, length, region } = this.state.filter;

        var lengthFilter = {
            "500": [0, 500],
            "3000": [500, 3000],
            "-1": [3000, Number.MAX_VALUE]
        };

        const projects = this.apiData
            .filter( project => {
                let located = region !== undefined && regions[region] !== undefined && project.region === region;
                return region ? located : project
            })
            .filter( project => type ? project.type === type : project)
            .filter( project => projectType ? project.projectType === projectType : project )
            .filter( project => {
                let cable = length !== undefined && parseInt(project.distance, 10) > lengthFilter[length][0] && parseInt(project.distance, 10) <= lengthFilter[length][1]
                return length ? cable : project
            })

        this.setState((prevState) => {
            return {projects: projects};
        });

        // clear lines when updating filters
        this._clearPolylines();

        // zoom out and display markers depending on filters
        this.onCloseCallout();

        // draw lines between filtered markers
        var that = this;
        projects.forEach(function(project) {
            that._drawPolylines(project.paths, project.completed);
        });

    }

    //handle pageination click
    handleClick = (event) => {
        event.preventDefault();
        this.setState({
            currentPage: Number(event.target.id)
        });
    }

    //pagination First link
    goToFirstPage = (event) => {
        event.preventDefault();
        this.setState({
            currentPage: 1
        });
    }

    //pagination < link
    goToPreviousPage = (event) => {
        event.preventDefault();
        let previousPage = 1;

        if (this.state.currentPage > 1) {
            previousPage = this.state.currentPage - 1;    
        }

        this.setState({
            currentPage: previousPage
        });
    }

    //pagination > link
    goToNextPage = (event) => {
        event.preventDefault();
        let lastPage = this.state.pageTotal;
        let nextPage = lastPage; //assume we are already on the last page

        if (this.state.currentPage < lastPage) {
            nextPage = this.state.currentPage + 1;    
        }
  
        this.setState({
            currentPage: nextPage
        });
    }

    //pagination Last link
    goToLastPage = (event) => {
        event.preventDefault();
        this.setState({
            currentPage: this.state.pageTotal
        });
    }
    
    //Add active class if it's truthy
    isActive = (number) => {
        return number === this.state.currentPage ? 'active' : '';
    }

    resetPagination = () => {
        console.log(this.state.projects.length)
        this.setState({
            currentPage: 1,
            projectsPerPage: 3,
            pageTotal: Math.ceil(this.state.projects.length / this.state.projectsPerPage)
        });
    }

    // clear marker connectors
    _clearPolylines = () => {
        this.polylines.forEach( polyline => {
            polyline.setMap(null)
        })
    }

    // drap polylines
    _drawPolylines = (paths, completionStatus) => {
        Object.keys(paths).forEach( plan => {
            let polyline = new this.maps.Polyline({
                path: paths[plan],
                geodesic: true,
                strokeColor: completionStatus ? '#00a7ea' : '#f39700',
                strokeOpacity: 1.0,
                strokeWeight: 2
            })

            // store a reference for deletion later
            this.polylines.push(polyline)

            polyline.setMap(this.map)
        });
    }

    // focus map to topic
    _fitMapToBounds = (paths) => {
        let bounds = new this.maps.LatLngBounds()
        Object.keys(paths).forEach( set => {
            paths[set].forEach(coord => {
                bounds.extend(coord)
            });
        });

        this.map.fitBounds(bounds)
    }

    // Add ability to run some window function on map initialization
    onMapInit = () => {
        // TODO: make configurable?
        if (window.HMN) {
            window.HMN.onMapInit && window.HMN.onMapInit(this)
            window.HMN._filterProducts = this.filterProjects
        }
    }

    // handle actions on map change
    onMapChange = (data) => {
        // console.log('event: map change')
    }

    // handle type dropdown selection
    onTypeSelect = (event) => {

        if (!event.target.value){
            this.setState({filter: Object.assign(delete this.state.filter.type) })
        } else {
            this.setState({filter: Object.assign(this.state.filter, { type: event.target.value})})
        }

        this.filterProjects();
    }

    // handle project type dropdown selection
    onProjectTypeSelect = (event) => {

        if (!event.target.value){
            this.setState({filter: Object.assign(delete this.state.filter.projectType) })
        } else {
            // console.log(event.target.value);
            this.setState({filter: Object.assign(this.state.filter, { projectType: event.target.value})})
        }

        this.filterProjects();
    }

    // handle project location dropdown selection
    onLocationSelect = (event) => {
        if (!event.target.value){
            this.setState({filter: Object.assign(delete this.state.filter.region) })
        } else {
            this.setState({filter: Object.assign(this.state.filter, { region: event.target.value})})
        }

        this.filterProjects();
    }

    // handle project length dropdown selection
    onLengthSelect = (event) => {
        if (!event.target.value){
            this.setState({filter: Object.assign(delete this.state.filter.length) })
        } else {
            this.setState({filter: Object.assign(this.state.filter, { length: event.target.value})})
        }

        this.filterProjects();
    }

    // handle clickin of marker
    onMarkerSelect = (project, ref) => {
        let refs = Object.keys(this.refs)
            .filter(key => key !== ref)
            .filter(key => key !== `map`)
            // close other callouts
            refs.forEach( key => {
                // console.log(key)
                this.refs[key].closeCallout()
            });

            // this.polylines
            // this._clearPolylines()

        // draw the polylines
        // this._drawPolylines(project.paths);

        //window.scrollTo(0, 0);
        //this.scrollToTop(500);

        // zoom the map
        this._fitMapToBounds(project.paths)
    }

    onCloseCallout = () => {
      this.setState({
        zoom: this.state.zoom === 3 ? 1 : 3
      });
    }

    scrollToTop = (scrollDuration) => {
      if(navigator.userAgent.match(/Trident\/7\./)) { // if IE
        window.scrollTo(0, 0);
      } else {
        const scrollHeight = window.scrollY,
          scrollStep = Math.PI / (scrollDuration / 15),
          cosParameter = scrollHeight / 2;
        var scrollCount = 0,
          scrollMargin,
          scrollInterval = setInterval(function() {
            if (window.scrollY !== 0) {
              scrollCount = scrollCount + 1;
              scrollMargin = cosParameter - cosParameter * Math.cos(scrollCount * scrollStep);
              window.scrollTo(0, (scrollHeight - scrollMargin));
            }
            else clearInterval(scrollInterval);
          }, 15);
      }
    }

    renderCards() {

        const indexOfLastProject = this.state.currentPage * this.state.projectsPerPage;
        const indexOfFirstProject = indexOfLastProject - this.state.projectsPerPage;
        const currentProjects = this.state.projects.slice(indexOfFirstProject, indexOfLastProject);

        if (currentProjects.length > 0) {
            return (
                currentProjects.map( (project, i) => {
                    const cardImagePath = window.MAPS_IMAGES_PATH || '/img/';
                    const redirect = window.MAPS_CARD_REDIRECT || project.redirect || '#';
                    return (
                      <div className="project-item col-xs-12 col-sm-4" key={project.id}>
                        <Card {...project}
                          key={project.id}
                          onClick={this.onMarkerSelect.bind(this, project)}
                          image={`${cardImagePath}${project.thumbnail}.jpg`}
                          redirect={redirect}
                        />
                      </div>
                    )
                  })
                  // this will break the row into 3 equal columns
                  .reduce( (r, element, index) => {
                    index % 3 === 0 && r.push([])
                    r[r.length - 1].push(element);
                    return r;
                  }, [])
                  .map( (card, i) => {
                    // return <div className="row" key={i}>{card}</div>;
                    return card;
                  })
            );
        }

        return (
            <div class="error--empty">
                <p>0 result found.</p>
            </div>
        );
    }

    render() {
        
        return (
            <div>
                <div className="map">
                    <GoogleMap
                        bootstrapURLKeys={{ key: this.apiKey, }}
                        onGoogleApiLoaded={({map, maps}) => {
                          this.map = map
                          this.maps = maps
                        }}
                        center={this.state.center}
                        zoom={this.state.zoom}
                        yesIWantToUseGoogleMapApiInternals
                        options={setMapOptions}
                        defaultCenter={this.props.center}
                        defaultZoom={this.props.zoom}
                        onChange={this.onMapChange}>
                        {
                          this.state.projects.map((project, index) => {
                            return project.locations &&
                              project.locations.map((location, i) => (
                                <Marker
                                  {...project}
                                  refKey={`marker-${index}-${i}`}
                                  ref={`marker-${index}-${i}`}
                                  lat={location.lat}
                                  lng={location.lng}
                                  fitMap={this._fitMapToBounds}
                                  onCalloutExpanded={this.onMarkerSelect.bind(this, project)}
                                  onCloseCallout={this.onCloseCallout}
                                />
                              ))
                          })
                        }
                    </GoogleMap>
                    <Legend/>
                </div>

                <div className="filters">
                    <div className="filter filter--type">
                        <select id="type" onChange={this.onTypeSelect} >
                            <option value="">All Project Cases</option>
                            {
                                this.state.types.map( (type, i) => {
                                    return <option key={type} value={type}>{type}</option>
                                })
                            }
                        </select>
                    </div>

                    <div className="filter filter--pType">
                        <select id="project_type" onChange={this.onProjectTypeSelect} >
                            <option value="">All Project Types</option>
                            {
                                this.state.pTyps.map( (type, i) => {
                                    return <option key={type} value={type}>{type}</option>
                                })
                            }
                        </select>
                    </div>
                    <div className="filter filter--location">
                        <select name="" id="project_location" onChange={this.onLocationSelect}>
                            <option value="">All Regions</option>
                            <option value="0">Asia-Pacific</option>
                            <option value="1">Europe, America, Latin America and Russia</option>
                            <option value="2">Pacific Islands</option>
                            <option value="3">Middle East and Africa</option>
                        </select>
                    </div>
                    <div className="filter filter--length">
                        <select name="" id="project_length" onChange={this.onLengthSelect}>
                            <option value="">All Lengths</option>
                            <option value="500">&lt; 500km</option>
                            <option value="3000">500-3000km</option>
                            <option value="-1">&gt; 3000km</option>
                        </select>
                    </div>
                </div>

                <div className="mapinfo">
                  {this.renderCards()}
                </div>

                <Pagination pageTotal={this.state.pageTotal}
                        currentPage={this.state.currentPage}
                        projectsPerPage={this.state.projectsPerPage}
                        goToFirstPage={this.goToFirstPage}
                        goToPreviousPage={this.goToPreviousPage}
                        goToLastPage={this.goToLastPage}
                        handleClick={this.handleClick}
                        goToNextPage={this.goToNextPage}
                        isActive={this.isActive}
                        resetPagination={this.resetPagination} />  
            
            </div>
        );
    }
}

export default App
