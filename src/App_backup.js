import React, { Component } from 'react'
import GoogleMap from 'google-map-react'

import './App.css';
import setMapOptions from './mapOptions';
import Card from './components/Card';
import Marker from './components/Marker';
import Legend from './components/Legend';

var regions = {
    "0" : "Asia-Pacific"
    "1" : "Europe, America, Latin America and Russia"
    "2" : "Pacific Islands"
    "3" : "Middle East and Africa"
};

class App extends Component {
    static defaultProps = {
        // set an initial map view
        center: { lat: 20.797201434307, lng: 22.8125 },
        zoom: 1
    }

    state = {
        filter: {},
        projects: [],
        locations: [],
        types: [],
        pTyps: [],
    }

    componentWillMount() {
        /*
         * !!! Requires Custom Map data and Google Maps API key !!!
         * A sample data structure can be found on `data.json` file
         */

        this.apiURL = window.MAPS_API_URL || 'http://www.mocky.io/v2/598055cf11000040091cf975';
        this.apiKey = window.MAPS_API_KEY || 'AIzaSyAhF9gZqAM0uksoaEJgMSglOLktMxrAhKg';

        // fetch json from api, please take note of the response format
        fetch(this.apiURL)
            .then(res => res.json())
            .then((data) => {
                this.apiData = data.body
                this.setState({'projects': data.body})
                this.polylines = []
                this.buildFilterOptions()

                this.apiData.forEach( project => {
                    this._drawPolylines(project.paths, project.completed);
                });
            }
        )
    }

    // dynamically build select options for filters
    buildFilterOptions = () => {
        const locs = [];
        const typs = [];
        const pTyps = [];

        // build locations dropdown options
        this.apiData.forEach( project => {
            project.locations.forEach( location => {
            if (locs.some( loc => loc.id === location.id )) return
                locs.push(location)
            })
        });

        // build project type dropdown options
        this.apiData.forEach( project => {

            if (pTyps.indexOf(project.projectType) < 0) {
                pTyps.push(project.projectType);
            }
        });

        this.apiData.forEach( project => {
            if (typs.indexOf(project.type) < 0) {
                typs.push(project.type);
            }
        });

        this.setState({ locations: locs, types: typs, pTyps: pTyps });
        this.onMapInit()
    }

    // ran after setting state filters
    filterProjects = () => {
        const { projectType, type, location, length } = this.state.filter;

        console.log(length);

        var lengthFilter = {
            "500": [0, 500],
            "3000": [500, 3000],
            "-1": [3000, Number.MAX_VALUE]
        };

        // console.log(parseInt(project.length) > lengthFilter[length][0]);

        const projects = this.apiData
            .filter( project => {
                let located = project.locations && project.locations.some( obj => obj.id === location )
                return location ? located : project
            })
            .filter( project => type ? project.type === type : project)
            .filter( project => projectType ? project.projectType === projectType : project )
            .filter( project => {
                let cable = length !== undefined && parseInt(project.distance) > lengthFilter[length][0] && parseInt(project.distance) <= lengthFilter[length][1]
                return length ? cable : project
            })

        this.setState({projects: projects})

        // clear lines when updating filters
        this._clearPolylines();

        // draw lines between filtered markers
        var that = this;
        projects.forEach(function(project) {
            that._drawPolylines(project.paths, project.completed);
        });
    }

    // clear marker connectors
    _clearPolylines = () => {
        this.polylines.forEach( polyline => {
            polyline.setMap(null)
        })
    }

    // drap polylines
    _drawPolylines = (paths, completionStatus) => {
        Object.keys(paths).forEach( plan => {
            let polyline = new this.maps.Polyline({
                path: paths[plan],
                geodesic: true,
                strokeColor: completionStatus ? '#00a7ea' : '#f39700',
                strokeOpacity: 1.0,
                strokeWeight: 2
            })

            // store a reference for deletion later
            this.polylines.push(polyline)

            polyline.setMap(this.map)
        });
    }

    // focus map to topic
    _fitMapToBounds = (paths) => {
        let bounds = new this.maps.LatLngBounds()
        Object.keys(paths).forEach( set => {
            paths[set].forEach(coord => {
                bounds.extend(coord)
            });
        });

        this.map.fitBounds(bounds)
    }

    // Add ability to run some window function on map initialization
    onMapInit = () => {
        // TODO: make configurable?
        if (window.HMN) {
            window.HMN.onMapInit && window.HMN.onMapInit(this)
            window.HMN._filterProducts = this.filterProjects
        }
    }

    // handle actions on map change
    onMapChange = (data) => {
        // console.log('event: map change')
    }

    // handle type dropdown selection
    onTypeSelect = (event) => {

        if (!event.target.value){
            this.setState({filter: Object.assign(delete this.state.filter.type) })
        } else {
            this.setState({filter: Object.assign(this.state.filter, { type: event.target.value})})
        }

        this.filterProjects();
    }

    // handle project type dropdown selection
    onProjectTypeSelect = (event) => {

        console.log(this.state);

        if (!event.target.value){
            this.setState({filter: Object.assign(delete this.state.filter.projectType) })
        } else {
            console.log(event.target.value);
            this.setState({filter: Object.assign(this.state.filter, { projectType: event.target.value})})
        }

        this.filterProjects();
    }

    // handle project location dropdown selection
    onLocationSelect = (event) => {
        if (!event.target.value){
            this.setState({filter: Object.assign(delete this.state.filter.location) })
        } else {
            this.setState({filter: Object.assign(this.state.filter, { location: event.target.value})})
        }

        this.filterProjects();
    }

    // handle project length dropdown selection
    onLengthSelect = (event) => {
        if (!event.target.value){
            this.setState({filter: Object.assign(delete this.state.filter.length) })
        } else {
            this.setState({filter: Object.assign(this.state.filter, { length: event.target.value})})
        }

        this.filterProjects();
    }

    // handle clickin of marker
    onMarkerSelect = (project, ref) => {
        let refs = Object.keys(this.refs)
            .filter(key => key !== ref)
            .filter(key => key !== `map`)

            // close other callouts
            refs.forEach( key => {
                console.log(key)
                this.refs[key].closeCallout()
            })

            // this.polylines
            // this._clearPolylines()

        // draw the polylines
        // this._drawPolylines(project.paths);

        // zoom the map
        this._fitMapToBounds(project.paths)
    }

    render() {
        return (
            <div>
                <div className="map">
                    <GoogleMap
                        bootstrapURLKeys={{ key: this.apiKey, }}
                        onGoogleApiLoaded={({map, maps}) => {
                          this.map = map
                          this.maps = maps
                        }}
                        yesIWantToUseGoogleMapApiInternals
                        options={setMapOptions}
                        defaultCenter={this.props.center}
                        defaultZoom={this.props.zoom}
                        onChange={this.onMapChange}>
                        {
                          this.state.projects.map((project, index) => {
                            return project.locations &&
                              project.locations.map((location, i) => (
                                <Marker
                                  {...project}
                                  refKey={`marker-${index}-${i}`}
                                  ref={`marker-${index}-${i}`}
                                  lat={location.lat}
                                  lng={location.lng}
                                  onCalloutExpanded={this.onMarkerSelect.bind(this, project)}
                                />
                              ))
                          })
                        }
                    </GoogleMap>
                    <Legend/>
                </div>

                <div className="filters">
                    <div className="filter filter--type">
                        <select id="type" onChange={this.onTypeSelect} >
                            <option value="">All Project Cases</option>
                            {
                                this.state.types.map( (type, i) => {
                                    return <option key={type} value={type}>{type}</option>
                                })
                            }
                        </select>
                    </div>

                    <div className="filter filter--pType">
                        <select id="project_type" onChange={this.onProjectTypeSelect} >
                            <option value="">All Project Types</option>
                            {
                                this.state.pTyps.map( (type, i) => {
                                    return <option key={type} value={type}>{type}</option>
                                })
                            }
                        </select>
                    </div>
                    <div className="filter filter--location">
                        <select name="" id="project_location" onChange={this.onLocationSelect}>
                            <option value="">All Regions</option>
                            {
                                this.state.locations.map( location => {
                                    return <option key={location.id} value={location.id}>{location.name}</option>
                                })
                            }
                        </select>
                    </div>
                    <div className="filter filter--length">
                        <select name="" id="project_length" onChange={this.onLengthSelect}>
                            <option value="">All Lengths</option>
                            <option value="500">&lt; 500km</option>
                            <option value="3000">500-3000km</option>
                            <option value="-1">&gt; 3000km</option>
                        </select>
                    </div>
                </div>

                <div className="mapinfo">
                  {
                    this.state.projects.map( (project, i) => {
                      const cardImagePath = window.MAPS_IMAGES_PATH || '/img/'
                      const redirect = window.MAPS_CARD_REDIRECT || project.redirect || '#'
                      console.log(project);
                      return (
                        <div className="col-md-4" key={project.id}>
                          <Card {...project}
                            key={project.id}
                            onClick={this.onMarkerSelect.bind(this, project)}
                            image={`${cardImagePath}${project.id}.jpg`}
                            redirect={redirect}
                          />
                        </div>
                      )
                    })
                    // this will break the row into 3 equal columns
                    .reduce( (r, element, index) => {
                      index % 3 === 0 && r.push([])
                      r[r.length - 1].push(element);
                      return r;
                    }, [])
                    .map( (card, i) => {
                      return <div className="row" key={i}>{card}</div>;
                    })
                  }
            </div>
        </div>
    );
  }
}

export default App
