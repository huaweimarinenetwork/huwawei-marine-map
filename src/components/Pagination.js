import React from 'react';

class Pagination extends React.Component {

    render() {

        const pageNumbers = [];
        const pageTotal = this.props.pageTotal;
    
        for (let i = 1; i <= pageTotal; i++) {
            pageNumbers.push(i);
        }

        if (pageTotal > 1) {

            return (
                <nav aria-label="Page navigation" className="text-center">
                    <ul className="pagination">
                        <li>
                            <a href="" onClick={this.props.goToFirstPage}>First</a>
                        </li>
                        <li>
                            <a href="" onClick={this.props.goToPreviousPage}>&lt;</a>
                        </li>
                            {
                                pageNumbers.map(number => {
                                    return (
                                        <li className={this.props.isActive(number)}>
                                            <a href=""
                                                key={number}
                                                id={number}
                                                onClick={this.props.handleClick}
                                            > {number} </a>
                                        </li>
                                    );
                                })
                            }
                        <li>
                            <a href="" onClick={this.props.goToNextPage}>&gt;</a>
                        </li>
                        <li>
                            <a href="" onClick={this.props.goToLastPage}>Last</a>
                        </li>
                    </ul>
                </nav>
            );
        }

        return null;
    }
}

export default Pagination;