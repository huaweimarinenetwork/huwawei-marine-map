import React, {Component} from 'react';

class Legend extends Component {


    render() {
        return (
            <div className="legend-container">
                <p className="legend-title">Legend</p>
                <div className="legend-content">
                    <ul>
                        <li>
                            <div className="legend-tile">
                                <img alt="Completed project location" src={"../../img/blue-marker.png"}/>
                            </div>
                            <div className="legend-name">Completed project location</div>
                        </li>
                        <li>
                            <div className="legend-tile">
                                <img alt="WIP project location" src={"../../img/orange-marker.png"}/>
                            </div>
                            <div className="legend-name">WIP project location</div>
                        </li>
                        <li>
                            <div className="legend-tile">
                                <img alt="Completed project cable route" src={"../../img/blue-line.png"}/>
                            </div>
                            <div className="legend-name">Completed project cable route</div>
                        </li>
                        <li>
                            <div className="legend-tile">
                                <img alt="Completed project cable route" src={"../../img/orange-line.png"}/>
                            </div>
                            <div className="legend-name">WIP project cable route</div>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Legend;
