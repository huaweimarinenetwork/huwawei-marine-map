import React from 'react';
import Truncate from 'react-truncate';

const Card = ({
    id,
    title,
    info,
    onClick,
    redirect,
    image,
    url
}) => {
    return (

        <div className="figure figure--animated">

            <div className="row">
                <div className="col-xs-12" onClick={onClick}>
                    <img className="img-responsive" src={image} alt={title}/>
                </div>
            </div>
            <a className="figure__title" href={url}>
                <div className="col-xs-12">
                    <h5>{title}</h5>
                    <p className="figure__info">
                        <Truncate lines={3} ellipsis={<span className="ellipsis">...>>></span>}>
                            {info}
                        </Truncate>
                    </p>
                </div>
            </a>
        </div>
    )
}

export default Card;
