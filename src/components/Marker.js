import React, { Component } from 'react'
var FontAwesome = require('react-fontawesome');

class Marker extends Component {
    state = {
        expanded: false
    }

    expandCallout = () => {
        this.setState({ expanded: !this.state.expanded })
        this.props.onCalloutExpanded(this.props.refKey)
    }

    closeCallout = () => {
        this.setState({ expanded: false })
        this.props.fitMap(0, 0);
    }

    // A mofified method to zoomout when x icon is clicked
    closeCalloutThenZoomOut = () => {
      this.setState({ expanded: false })
      this.props.fitMap(0, 0);
      //this.props.onCloseCallout();
    }

    render() {
        const { title, completed, distance, RFS, system, url } = this.props
        let markerStyle = completed ? {} : { backgroundImage: 'url(../img/orange-marker-large.png)' };
        let show = this.state.expanded ? { display: 'block', opacity: 1 } : {}

        return (
            <div className="marker" style={markerStyle} onClick={this.expandCallout} >
                <div style={show} className="marker__callout">
                    <a className="marker-close" onClick={this.closeCalloutThenZoomOut}>
                        <FontAwesome name="times"/>
                    </a>
                    <a href={url} className="marker__title">
                        {title}
                    </a>
                    <p className="marker__system-type">{system}</p>
                    <p className="marker__cable-length">{distance}</p>
                    <p className="marker__RFS">{RFS}</p>
                </div>
            </div>
        )
    }
}

export default Marker
