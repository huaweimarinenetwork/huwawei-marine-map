# REAME

*Todos*

- [x] development instructions
- [x] integration
- [ ] data development

## Development

#### Requires

- [Node](https://nodejs.org)

To get started, simply pop open a terminal and navigate to the project folder (where this readme resides) and run the following lines over.

```bash
npm run start
```

This will automatically open up the browser at `http://localhost:3000`


## Integration

#### Requires

- [Bootstrap](https://getbootstrap.com)

Bootstrap takes care of the grid alignment of the cards, as well as mobile responsiveness.

#### Distribution

The entire component can be extracted by running the command

```bash
npm run build
```

This will generate the necessary files over at the `dist` folder.

Simply link these files over to a template that should have an html node with an id `root`

```html
<!doctype html>
<html lang="en">
<head>
  <!-- ... -->
  <script src="../some/path/main.892f44af.js"></script>
</head>
<body>
  <!-- ... -->
  <div id="root"></div>
</body>
</html>
```

## Data Development
